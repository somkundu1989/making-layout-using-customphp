<!-- #######  YAY, I AM THE SOURCE EDITOR! #########-->
<div style="color: #000000; background-color: #fffffe; font-family: Consolas, 'Courier New', monospace; font-weight: normal; font-size: 14px; line-height: 19px; white-space: pre;">
<div><span style="color: #000000;">Source: https://gitlab.com/somkundu1989/making-layout-using-customphp</span></div>
<div><span style="color: #000000;">download link is there to download as zip</span></div>
<div><span style="color: #000000;">in case you download; extract it to xampp/htdocs or wamp/www or somewhere you specified to run your php projects</span></div>
<br />
<div><span style="color: #0000ff;">-----------------------------------------------------------------------------------</span></div>
<div><span style="color: #000000;">How to run:</span></div>
<div><span style="color: #000000;">/layout/samples/common.php ( http://localhost/making-layout-using-customphp/layout/samples/common.php)</span></div>
<div><span style="color: #0000ff;">------------------------------------------------------------------------------------</span></div>
<div><span style="color: #000000;">What to learn:</span></div>
<div><span style="color: #0000ff;">1. </span><span style="color: #000000;">php starts with &lt;?php , &lt;?=$variable?&gt; and &lt;?php echo $variable; ?&gt; is same both are used to echo </span></div>
<div><span style="color: #0000ff;">2. </span><span style="color: #000000;">env file -&gt; which is used to store static values</span></div>
<br /><br />
<div><span style="color: #000000;">&lt;?php</span></div>
<div><span style="color: #000000;">$base_url='http://localhost/making-layout-using-customphp/';</span></div>
<div><span style="color: #000000;">$asset_url=$base_url.'public/admin/';</span></div>
<div><span style="color: #000000;">$public_url=$base_url.'public/';</span></div>
<br />
<div><span style="color: #000000;">$servername = "localhost";</span></div>
<div><span style="color: #000000;">$username = "root";</span></div>
<div><span style="color: #000000;">$password = "";</span></div>
<div><span style="color: #000000;">$dbname = "virtual_school";</span></div>
<br />
<div><span style="color: #000000;">env file can be renamed with anything you like </span></div>
<div><span style="color: #0000ff;">3. </span><span style="color: #000000;">For a bootstrap template all assets is placed into /public/admin folder hence the asset link will be </span></div>
<br /><br />
<div><span style="color: #800000;">&lt;link</span> <span style="color: #ff0000;">rel</span><span style="color: #383838;">=</span><span style="color: #0000ff;">"stylesheet"</span> <span style="color: #ff0000;">href</span><span style="color: #383838;">=</span><span style="color: #0000ff;">"http://localhost/making-layout-using-customphp/public/admin/plugins/fontawesome-free/css/all.min.css"</span><span style="color: #800000;">&gt;</span></div>
<div><span style="color: #800000;">&lt;link</span> <span style="color: #ff0000;">rel</span><span style="color: #383838;">=</span><span style="color: #0000ff;">"stylesheet"</span> <span style="color: #ff0000;">href</span><span style="color: #383838;">=</span><span style="color: #0000ff;">"http://localhost/making-layout-using-customphp/public/admin/dist/css/adminlte.min.css"</span><span style="color: #800000;">&gt;</span></div>
<br />
<div><span style="color: #800000;">&lt;img</span> <span style="color: #ff0000;">src</span><span style="color: #383838;">=</span><span style="color: #0000ff;">"http://localhost/making-layout-using-customphp/public/admin/dist/img/AdminLTELogo.png"</span><span style="color: #800000;">&gt;</span></div>
<br />
<div><span style="color: #800000;">&lt;script</span> <span style="color: #ff0000;">src</span><span style="color: #383838;">=</span><span style="color: #0000ff;">"http://localhost/making-layout-using-customphp/public/admin/plugins/jquery/jquery.min.js"</span><span style="color: #800000;">&gt;&lt;/script&gt;</span></div>
<div><span style="color: #800000;">&lt;script</span> <span style="color: #ff0000;">src</span><span style="color: #383838;">=</span><span style="color: #0000ff;">"http://localhost/making-layout-using-customphp/public/admin/dist/js/adminlte.min.js"</span><span style="color: #800000;">&gt;&lt;/script&gt;</span></div>
<br />
<div><span style="color: #000000;">Here the observable &ldquo;http://localhost/making-layout-using-customphp/public/admin/&rdquo; is everywhere, now here is the explanation:</span></div>
<div><span style="color: #000000;">a. Base url is &ldquo;http://localhost/making-layout-using-customphp&rdquo; as &ldquo;making-layout-using-customphp&rdquo; folder is stored in htdocs/www folder.</span></div>
<div><span style="color: #000000;">b. &ldquo;public/admin/&rdquo; folder where all necessary css &amp; js files are stored hence &ldquo;http://localhost/making-layout-using-customphp/public/admin/&rdquo; is important</span></div>
<br /><br /><br /><br /><br /></div>
<p><strong>&nbsp;</strong></p>